#!/bin/sh

VIMRC=~/.vimrc
IDEAVIMRC=~/.ideavimrc
VIMDIR=~/.vim
AUTOLOAD="$VIMDIR/autoload"
BUNDLE="$VIMDIR/bundle"
COLORS="$VIMDIR/colors"
FTPLUGIN="$VIMDIR/ftplugin"

if [ -e $VIMDIR ]
then
    cp -pr $VIMDIR $VIMDIR".bak_$(date +'%s')"
fi

if [ -e $VIMRC ]
then
    cp -pr $VIMRC $VIMRC".bak_$(date +'%s')"
fi

if [ -e $IDEAVIMRC ]
then
    cp -pr $IDEAVIMRC $IDEAVIMRC".bak_$(date +'%s')"
fi

rm -rf $VIMDIR
rm -rf $VIMRC
rm -rf $IDEAVIMRC

# Prepare directories for plugins.
mkdir -p $AUTOLOAD $BUNDLE $COLORS $FTPLUGIN

sudo apt-get update
sudo apt-get install -y vim-gtk silversearcher-ag exuberant-ctags

# Install .vimrc.
wget -qO $VIMRC https://raw.githubusercontent.com/korzeniewskipl/vimcharm/master/.vimrc

# Install .ideavimrc.
wget -qO $IDEAVIMRC https://raw.githubusercontent.com/korzeniewskipl/vimcharm/master/.ideavimrc

# Install pathogen.
wget -qO $AUTOLOAD/pathogen.vim https://tpo.pe/pathogen.vim

# Install color schemes.
wget -qO $COLORS/apprentice.vim https://raw.githubusercontent.com/korzeniewskipl/vimcharm/master/.vim/colors/apprentice.vim
wget -qO $COLORS/hybrid.vim https://raw.githubusercontent.com/korzeniewskipl/vimcharm/master/.vim/colors/hybrid.vim
wget -qO $COLORS/wombat256mod.vim https://raw.githubusercontent.com/korzeniewskipl/vimcharm/master/.vim/colors/wombat256mod.vim
git clone git://github.com/altercation/vim-colors-solarized.git $BUNDLE/vim-colors-solarized

# Install powerline.
git clone https://github.com/powerline/powerline.git $BUNDLE/powerline

# Install powerline fonts.
git clone https://github.com/powerline/fonts.git $VIMDIR/fonts
$VIMDIR/fonts/install.sh

# Install ctrlp.vim.
git clone https://github.com/kien/ctrlp.vim.git $BUNDLE/ctrlp.vim

# Install nerdcommenter.
git clone https://github.com/scrooloose/nerdcommenter.git $BUNDLE/nerdcommenter

# Install nerdtree.
git clone https://github.com/scrooloose/nerdtree.git $BUNDLE/nerdtree

# Install python-mode.
git clone https://github.com/klen/python-mode.git $BUNDLE/python-mode

# Install vim-ansible-yaml.
git clone https://github.com/chase/vim-ansible-yaml.git $BUNDLE/vim-ansible-yaml

# Install vim-gitgutter.
git clone https://github.com/airblade/vim-gitgutter.git $BUNDLE/vim-gitgutter

# Install vim-fugitive.
git clone https://github.com/tpope/vim-fugitive.git $BUNDLE/vim-fugitive

# Install riv.vim.
git clone https://github.com/Rykka/riv.vim.git $BUNDLE/riv.vim

# Install tagbar.
git clone https://github.com/majutsushi/tagbar.git $BUNDLE/tagbar

# Install jedi-vim.
git clone https://github.com/davidhalter/jedi-vim.git $BUNDLE/jedi-vim

# Install supertab.
git clone https://github.com/ervandew/supertab.git $BUNDLE/supertab
